package QuestionsAndAnswers;

public class Sport extends QuestionsAndAnswers {
	
	public Sport() {
		
//Questions:
		questionList[0][0]="SportQuestion1";
		questionList[1][0]="SportQuestion2";
		questionList[2][0]="SportQuestion3";
		
		questionList[3][0]="SportQuestion4";
		questionList[4][0]="SportQuestion5";
		questionList[5][0]="SportQuestion6";
		
		questionList[6][0]="SportQuestion7";
		questionList[7][0]="SportQuestion8";
		questionList[8][0]="SportQuestion9";
		
		
//Answers:
//-------------Easy-----------
		questionList[0][1]="SportEasyAnswer1a";
		questionList[0][2]="SportEasyAnswer1b";
		questionList[0][3]="SportEasyAnswer1c";
		questionList[0][4]="SportEasyAnswer1d";
		
		questionList[1][1]="SportEasyAnswer2a";
		questionList[1][2]="SportEasyAnswer2b";
		questionList[1][3]="SportEasyAnswer2c";
		questionList[1][4]="SportEasyAnswer2d";
		
		questionList[2][1]="SportEasyAnswer3a";
		questionList[2][2]="SportEasyAnswer3b";
		questionList[2][3]="SportEasyAnswer3c";
		questionList[2][4]="SportEasyAnswer3d";
		
//-----------Medium-----------
		
		questionList[3][1]="SportMediumAnswer1a";
		questionList[3][2]="SportMediumAnswer1b";
		questionList[3][3]="SportMediumAnswer1c";
		questionList[3][4]="SportMediumAnswer1d";
		
		questionList[4][1]="SportMediumAnswer2a";
		questionList[4][2]="SportMediumAnswer2b";
		questionList[4][3]="SportMediumAnswer2c";
		questionList[4][4]="SportMediumAnswer2d";
		
		questionList[5][1]="SportMediumAnswer3a";
		questionList[5][2]="SportMediumAnswer3b";
		questionList[5][3]="SportMediumAnswer3c";
		questionList[5][4]="SportMediumAnswer3d";
		
//------------Hard------------
		
		questionList[6][1]="SportHardAnswer1a";
		questionList[6][2]="SportHardAnswer1b";
		questionList[6][3]="SportHardAnswer1c";
		questionList[6][4]="SportHardAnswer1d";
		
		questionList[7][1]="SportHardAnswer2a";
		questionList[7][2]="SportHardAnswer2b";
		questionList[7][3]="SportHardAnswer2c";
		questionList[7][4]="SportHardAnswer2d";
		
		questionList[8][1]="SportHardAnswer3a";
		questionList[8][2]="SportHardAnswer3b";
		questionList[8][3]="SportHardAnswer3c";
		questionList[8][4]="SportHardAnswer3d";
				
	}
	
	@Override
	public String getCorrectAnswer(int i) {
		String correctAnswer = new String();
		for(int j=1 ; j<5 ; j++) {
			if(questionList[i][j].contains("!")) {
				correctAnswer = questionList[i][j];
			}
		}
		return correctAnswer;
	}
	
	public  String[][] getQuestionList(){
		return drawAnswers(questionList);
	}
	
	

}
