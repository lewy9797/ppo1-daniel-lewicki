package QuestionsAndAnswers;

public class Physics extends QuestionsAndAnswers{
	
	public Physics() {
		
//Questions:
		questionList[0][0]="PhysicsQuestion1";
		questionList[1][0]="PhysicsQuestion2";
		questionList[2][0]="PhysicsQuestion3";
		
		questionList[3][0]="PhysicsQuestion4";
		questionList[4][0]="PhysicsQuestion5";
		questionList[5][0]="PhysicsQuestion6";
		
		questionList[6][0]="PhysicsQuestion7";
		questionList[7][0]="PhysicsQuestion8";
		questionList[8][0]="PhysicsQuestion9";
		
		
//Answers:
//-------------Easy-----------
		questionList[0][1]="PhysicsEasyAnswer1a";
		questionList[0][2]="PhysicsEasyAnswer1b";
		questionList[0][3]="PhysicsEasyAnswer1c";
		questionList[0][4]="PhysicsEasyAnswer1d";
		
		questionList[1][1]="PhysicsEasyAnswer2a";
		questionList[1][2]="PhysicsEasyAnswer2b";
		questionList[1][3]="PhysicsEasyAnswer2c";
		questionList[1][4]="PhysicsEasyAnswer2d";
		
		questionList[2][1]="PhysicsEasyAnswer3a";
		questionList[2][2]="PhysicsEasyAnswer3b";
		questionList[2][3]="PhysicsEasyAnswer3c";
		questionList[2][4]="PhysicsEasyAnswer3d";
		
//-----------Medium-----------
		
		questionList[3][1]="PhysicsMediumAnswer1a";
		questionList[3][2]="PhysicsMediumAnswer1b";
		questionList[3][3]="PhysicsMediumAnswer1c";
		questionList[3][4]="PhysicsMediumAnswer1d";
		
		questionList[4][1]="PhysicsMediumAnswer2a";
		questionList[4][2]="PhysicsMediumAnswer2b";
		questionList[4][3]="PhysicsMediumAnswer2c";
		questionList[4][4]="PhysicsMediumAnswer2d";
		
		questionList[5][1]="PhysicsMediumAnswer3a";
		questionList[5][2]="PhysicsMediumAnswer3b";
		questionList[5][3]="PhysicsMediumAnswer3c";
		questionList[5][4]="PhysicsMediumAnswer3d";
		
//------------Hard------------
		
		questionList[6][1]="PhysicsHardAnswer1a";
		questionList[6][2]="PhysicsHardAnswer1b";
		questionList[6][3]="PhysicsHardAnswer1c";
		questionList[6][4]="PhysicsHardAnswer1d";
		
		questionList[7][1]="PhysicsHardAnswer2a";
		questionList[7][2]="PhysicsHardAnswer2b";
		questionList[7][3]="PhysicsHardAnswer2c";
		questionList[7][4]="PhysicsHardAnswer2d";
		
		questionList[8][1]="PhysicsHardAnswer3a";
		questionList[8][2]="PhysicstHardAnswer3b";
		questionList[8][3]="PhysicsHardAnswer3c";
		questionList[8][4]="PhysicsHardAnswer3d";
		
	}
	
	@Override
	public String getCorrectAnswer(int i) {
		String correctAnswer = new String();
		for(int j=1 ; j<5 ; j++) {
			if(questionList[i][j].contains("!")) {
				correctAnswer = questionList[i][j];
			}
		}
		return correctAnswer;
	}
	
	public  String[][] getQuestionList(){
		return drawAnswers(questionList);
	}
	
	

}
