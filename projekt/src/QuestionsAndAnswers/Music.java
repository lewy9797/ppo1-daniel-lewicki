package QuestionsAndAnswers;

public class Music extends QuestionsAndAnswers{
	
	public Music() {
		
//Questions:
		questionList[0][0]="ArtQuestion1";
		questionList[1][0]="ArtQuestion2";
		questionList[2][0]="ArtQuestion3";
		
		questionList[3][0]="ArtQuestion4";
		questionList[4][0]="ArtQuestion5";
		questionList[5][0]="ArtQuestion6";
		
		questionList[6][0]="ArtQuestion7";
		questionList[7][0]="ArtQuestion8";
		questionList[8][0]="ArtQuestion9";
		
		
//Answers:
//-------------Easy-----------
		questionList[0][1]="ArtEasyAnswer1a";
		questionList[0][2]="ArtEasyAnswer1b";
		questionList[0][3]="ArtEasyAnswer1c";
		questionList[0][4]="ArtEasyAnswer1d";
		
		questionList[1][1]="ArtEasyAnswer2a";
		questionList[1][2]="ArtEasyAnswer2b";
		questionList[1][3]="ArtEasyAnswer2c";
		questionList[1][4]="ArtEasyAnswer2d";
		
		questionList[2][1]="ArtEasyAnswer3a";
		questionList[2][2]="ArtEasyAnswer3b";
		questionList[2][3]="ArtEasyAnswer3c";
		questionList[2][4]="ArtEasyAnswer3d";
		
//-----------Medium-----------
		
		questionList[3][1]="ArtMediumAnswer1a";
		questionList[3][2]="ArtMediumAnswer1b";
		questionList[3][3]="ArtMediumAnswer1c";
		questionList[3][4]="ArtMediumAnswer1d";
		
		questionList[4][1]="ArtMediumAnswer2a";
		questionList[4][2]="ArtMediumAnswer2b";
		questionList[4][3]="ArtMediumAnswer2c";
		questionList[4][4]="ArtMediumAnswer2d";
		
		questionList[5][1]="ArtMediumAnswer3a";
		questionList[5][2]="ArtMediumAnswer3b";
		questionList[5][3]="ArtMediumAnswer3c";
		questionList[5][4]="ArtMediumAnswer3d";
		
//------------Hard------------
		
		questionList[6][1]="ArtHardAnswer1a";
		questionList[6][2]="ArtHardAnswer1b";
		questionList[6][3]="ArtHardAnswer1c";
		questionList[6][4]="ArtHardAnswer1d";
		
		questionList[7][1]="ArtHardAnswer2a";
		questionList[7][2]="ArtHardAnswer2b";
		questionList[7][3]="ArtHardAnswer2c";
		questionList[7][4]="ArtHardAnswer2d";
		
		questionList[8][1]="ArtHardAnswer3a";
		questionList[8][2]="ArtHardAnswer3b";
		questionList[8][3]="ArtHardAnswer3c";
		questionList[8][4]="ArtHardAnswer3d";
		
	}
	
	@Override
	public String getCorrectAnswer(int i) {
		String correctAnswer = new String();
		for(int j=1 ; j<5 ; j++) {
			if(questionList[i][j].contains("!")) {
				correctAnswer = questionList[i][j];
			}
		}
		return correctAnswer;
	}
	
	public  String[][] getQuestionList(){
		return drawAnswers(questionList);
	}
	
	

}
