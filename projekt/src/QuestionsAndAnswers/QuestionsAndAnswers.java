package QuestionsAndAnswers;
import java.util.Scanner;
import java.util.Random;

public abstract class QuestionsAndAnswers {
	
	protected QuestionsAndAnswers() {}
	static Scanner scan = new Scanner(System.in);
	private Random r = new Random();
	protected String[][] questionList = new String[9][5];
	
	abstract public String getCorrectAnswer(int i);   //metoda zwraca poprawn� odpowiedz
	abstract public String[][] getQuestionList();     //metoda zwraca list� z pytaniami i odpowiedziami

	//metoda zwraca macierz z pseudo-losowo umieszczonymi w macierzy odpowiedziami 
	protected String[][] drawAnswers(String[][] list){
		for(int i=0 ; i<9 ; i++) {
			for(int j=1 ; j<5 ; j++) {
				int ans1 = r.nextInt(4)+1;
				int ans2 = r.nextInt(4)+1;
				while(ans1 == ans2) {
					ans2 = r.nextInt(4)+1;
				}
				
				String answer = new String(list[i][ans2]);
				list[i][ans2] = list[i][ans1];
				list[i][ans1] = answer;
			}
		}
		return list;
	}
	
	//zwraca list� pyta� i odpowiedzi w zale�no�ci od podanego poziomu trudno�ci
	public String[][] chooseLevel(int level) {
		
		String[][] list1 = getQuestionList();
		String[][] list2 = new String[3][5];
		switch(level) {
		
		case 1:
			for(int i=0 ; i<3 ; i++) {
				for(int j=0 ; j<5 ; j++) {
					list2[i][j] = list1[i][j];
				}
			}
			
			break;
			
		case 2:
			for(int i=3 ; i<6 ; i++) {
				for(int j=0 ; j<5 ; j++) {
					list2[i-3][j] = list1[i][j];
				}
			}
			
			break;
			
		case 3:
			for(int i=6 ; i<9 ; i++) {
				for(int j=0 ; j<5 ; j++) {
					list2[i-6][j] = list1[i][j];
				}
			}
			
			break;
	
		}
		
		return list2;
		
	}
	//losuje i zwraca wylosowane pytanie z podanego pakietu pyta�
	public int getYourPack(int i) {
		int nr=0;
		
		if(i == 1) {
			nr = r.nextInt(3);
		}
		if(i == 2) {
			nr = r.nextInt(3)+3;
		}
		if(i == 3) {
			nr = r.nextInt(3)+6;
		}
		return nr;
	}

}
