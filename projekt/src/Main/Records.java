package Main;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Records{
	 private String txt;
	    private String Enter = System.getProperty("line.separator");
	    final private String goodWord, directory;
	    private int global = 10;

	    public Records(String s1, String dir){
	    	goodWord = s1;
	        directory = dir;
	        Create();
	        loadFromFile();
	        saveToFile();
	    }

	    private void Create()
	    {
	         try {   
	    		File file = new File("Records.txt");
	            if(!file.exists()) {
	                file.createNewFile();
	                FileWriter fill = new FileWriter("Records.txt", true);
	                BufferedWriter add = new BufferedWriter(fill);
	                String[] tablica = new String [] {"1000000", "500000", "250000", "100000", "50000", "25000", "10000", "5000", "2500", "1000"};
	                for(int i=0; i<global; i++)
	                {
	                	add.write(tablica[i]);
	                	add.write(Enter);
	                }
	                add.close();
	            }
	         } catch (IOException e) {
	             e.printStackTrace();
	         }
	    }
	    
	    private void loadFromFile(){
	    	int good=0, bad=0, change=0, done=0;

	        txt = "";
	        BufferedReader br;
	        String line, changestr, badstr;
	        
	        try {       
	        br = new BufferedReader(new FileReader(directory));
	       
	        while((line=br.readLine())!=null)
	        {
	        	
	        	bad = Integer.parseInt(line);
	    		good = Integer.parseInt(goodWord);

	        	if (good > bad)
	        	{
	        		
	        		bad = Integer.parseInt(line);
	        		good = Integer.parseInt(goodWord);

	        		change = good;
	        	       		
	        		changestr = Integer.toString(change);
	        		badstr = Integer.toString(bad);

	        		txt += line.replace(badstr, changestr);
	        		txt += Enter;
	        		done++;
	        		break;
	        	}
	        	else
	        	{
	        		txt += line;
	        		txt += Enter;
	        	}
	        	if(done==1)
	        	{
	        		System.out.print("Hej");
	        		txt += line;
	        		txt += Enter;
	        	}
	        }
	        done=0;
	        
	            br.close();
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }

	    private void saveToFile(){
	    	
	        BufferedWriter bw;

	        try {
	            bw = new BufferedWriter(new FileWriter(directory));
	            bw.write(txt);
	            bw.flush();
	            bw.close();
	            
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
		
	    
	}