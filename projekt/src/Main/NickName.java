package Main;

import java.util.regex.Pattern;
import java.util.Scanner;

public interface NickName {
	default String getNickName(String input) {
		
		Pattern EMAIL = Pattern.compile("[a-zA-Z0-9-_.]+@[a-z0-9-.]+.[a-z0-9]{1,4}");
		Scanner scan = new Scanner(System.in);
		while(!EMAIL.matcher(input).matches()) {
			Log.info("Niepoprawny adres mailowy!\n(nazwa@adresStrony.domena)");
			Log.info("Podaj email: ");
			input = scan.next();
			scan.close();
		}	
		return input;
	}
}
