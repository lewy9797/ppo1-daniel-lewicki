package Main;
import java.util.ArrayList;
import LifeLines.*;
import QuestionsAndAnswers.*;
import java.util.Collections;
import java.util.Scanner;
import java.util.Random;

final public class Quiz implements QuizEntry, NickName{

	private boolean isRun = false;
	private static Random r = new Random();
	private Scanner scan= new Scanner(System.in);
	private ArrayList<QuestionsAndAnswers> categories = new ArrayList();
	private ArrayList<LifeLines> lifeLines = new ArrayList();
	private String[][] list = new String[9][5];
	private String nickName =  null;
	private int score=0;
	
	public Quiz() {
		Log.info("Rozpoczynamy Quiz!");
		Log.info("Podaj email: ");
		nickName = scan.next();
		getNickName();
		isRun = true;
	}

	protected void startQuiz() {
		if(!isRun) {
			Log.info("Co� poszlo nie tak ...");
		}
		else {
			
			fillQandA();
			lifeLines = fillLifeLines();
			int questionNumber=1;
			while(isRun) {			
			
				Log.info("Podaj Poziom Trudno�ci pytania nr ["+questionNumber+"] w zakresie:(1-3)");
				int lvl = scan.nextInt();
				while(!(lvl >= 1 && lvl <= 3)) {
					Log.info("Nieprawid�owy poziom! Podaj liczb� od 1 do 3!");
					lvl = scan.nextInt();
				}
				Log.info("Wybra�e� lvl: "+lvl);
				
				list = categories.get(questionNumber-1).getQuestionList();
				int question = categories.get(questionNumber-1).getYourPack(lvl);
				Log.info("Pytanie nr ["+questionNumber+"]:\n"+list[question][0]);
				//Drukowanie odpowiedzi:
				printAnswers(list,question);
				if(!lifeLines.isEmpty()) {
					Log.info("Wybierz ko�o ratunkowe lub wybierz 4 je�li nie chcesz skorzysta� z k�.");
					printLifeLines();
					int lifeLineNumber = scan.nextInt();
					
					//metoda sprawdzajaca
					if(lifeLineNumber == lifeLines.size()+1) {
						Log.info("Nie skorzysta�e� z ko�a ratunkowego");
					}
					else {
						String[][] helpList = new String[9][5];
						helpList = list;
						list = lifeLines.get(lifeLineNumber-1).lifeLine(helpList,question,categories.get(questionNumber-1));
						
						if(lifeLines.get(lifeLineNumber-1) instanceof SecondChance) {
							question = lifeLines.get(lifeLineNumber-1).getNewQuestion();
							Log.info("Pytanie nr ["+questionNumber+"]po drugiej szansie:\n"+list[question][0]);
							printAnswers(list,question);
							
						}
						if(lifeLines.get(lifeLineNumber-1) instanceof FiftyFifty) {
							printAnswers(list,question);
							
						}
						if(lifeLines.get(lifeLineNumber-1) instanceof ChangeOfCategory) {
							Log.info("Pytanie nr ["+questionNumber+"]po zmianie kategorii:\n"+list[question][0]);
							printAnswers(list,question);
							categories.add(lifeLines.get(lifeLineNumber-1).getCategories().get(lifeLines.get(lifeLineNumber-1).getCategories().size()-1));
							
							categories.set(questionNumber-1, categories.get(categories.size()-1));
							
						}
						lifeLines.remove(lifeLines.get(lifeLineNumber-1));
					}
				}
				
				else {
					Log.info("Wykorzystales wszystkie kola ratunkowe!");
				}
				
				
				
				
				
				
				Log.info("Podaj Odpowiedz");
				int ans = scan.nextInt();
				
				
				while(!(ans == 1 || ans == 2 || ans == 3 || ans == 4)) {
					Log.info("Podaj Odpowiedz 1,2,3 lub 4!");
					ans = scan.nextInt();
				}
				
				switch(ans) {
					
				case 1:
					if(list[question][1].contentEquals(categories.get(questionNumber-1).getCorrectAnswer(question)) ) {			
						score = getScore(lvl,questionNumber);
						Log.info("Prawid�owa Odpowiedz!\nWynik: "+score);
						
					}
					else {
						Log.info("Niestety. Spr�buj jeszcze raz!");
						Log.info("Tw�j wynik: "+score);
						isRun = false;
					}
					break;
				case 2:
					if(list[question][2].contentEquals(categories.get(questionNumber-1).getCorrectAnswer(question))) {						
						score = getScore(lvl,questionNumber);
						Log.info("Prawid�owa Odpowiedz!\nWynik: "+score);
					}
					else {
						Log.info("Niestety. Spr�buj jeszcze raz!");
						Log.info("Tw�j wynik: "+score);
						isRun = false;
					}
					break;
				case 3:
					if(list[question][3].contentEquals(categories.get(questionNumber-1).getCorrectAnswer(question))) {						
						score = getScore(lvl,questionNumber);
						Log.info("Prawid�owa Odpowiedz!\nWynik: "+score);
					}
					else {
						Log.info("Niestety. Spr�buj jeszcze raz!");
						Log.info("Tw�j wynik: "+score);
						isRun = false;
					}
					break;
				case 4:
					if(list[question][4].contentEquals(categories.get(questionNumber-1).getCorrectAnswer(question))) {						
						score = getScore(lvl,questionNumber);
						Log.info("Prawid�owa Odpowiedz!\nWynik: "+score);
					}
					else {
						Log.info("Niestety. Spr�buj jeszcze raz!");
						Log.info("Tw�j wynik: "+score);
						isRun = false;
					}
					break;
				}
			
				if(questionNumber > 9) {
					Log.info("Wygra�e� 1 000 000 cebulion�w!");
					isRun = false;
				}
				else {
					questionNumber++;
				}
			
			}	
		}
	}
	//metoda wype�nia list� k� ratunkowych
	private  ArrayList<LifeLines> fillLifeLines(){
		lifeLines.add(new FiftyFifty());
		lifeLines.add(new ChangeOfCategory());
		lifeLines.add(new SecondChance());
		return lifeLines;
		
	}
	//metoda wype�nia list� kategorii
	private  ArrayList<QuestionsAndAnswers> fillQandA(){
				
		categories.add(new Biology());
		categories.add(new Art());
		categories.add(new Film());
		categories.add(new Geography());
		categories.add(new It());
		categories.add(new MathClass());
		categories.add(new Music());
		categories.add(new Physics());
		categories.add(new Sport());
		
		Collections.shuffle(categories);
		return categories;
	}
	//metoda wypisuje odpowiedzi na ekran
	private static void printAnswers(String[][] list, int j) {
		for(int i=1 ; i<5 ; i++) {
			Log.info(i+") "+list[j][i].replace("!", ""));
		}
		
	}
	//metoda generuje szereg wynik�w dla �atwych pyta�
	private static int getEasyScore(int questionNumber) {
		int[] score = new int[10];
		score[0] = 500;
		
		for(int i = 1 ; i<10 ; i++) {
			if(i == 1 || i == 4 || i == 7 ) {
				score[i] = (int)(score[i-1]*2.5);
			}
			else {
				score[i] = score[i-1] * 2;
			}
		}
		return score[questionNumber-1];
	}
	//metoda generuje szereg wynik�w dla �rednich pyta�
	private static int getMediumScore(int questionNumber) {
		int[] score = new int[10];
		score[0] = 750;
		
		for(int i = 1 ; i<10 ; i++) {
			if(i == 1 || i == 4 || i == 7 ) {
				score[i] = (int)(score[i-1]*2.5);
			}
			else {
				score[i] = score[i-1] * 2;
			}
		}
		return score[questionNumber-1];
	}
	//metoda generuje szereg wynik�w dla trudnych pyta�
	private static int getHardScore(int questionNumber) {
		int[] score = new int[10];
		score[0] = 1000;
		
		for(int i = 1 ; i<10 ; i++) {
			if(i == 1 || i == 4 || i == 7 ) {
				score[i] = (int)(score[i-1]*2.5);
			}
			else {
				score[i] = score[i-1] * 2;
			}
		}
		return score[questionNumber-1];
	}
	//metoda dokonuje decyzji kt�ry wynik zwr�ci� z szeregu punktancji 
	private static int getScore(int lvl, int questionNumber) {
		int score = 0 ;
		switch(lvl) {
		case 1:
			score = getEasyScore(questionNumber);
			break;
		case 2:
			score = getMediumScore(questionNumber);
			break;
		case 3:
			score = getHardScore(questionNumber);
			break;
		}
		return score;
	}
	//metoda wy�wietla na ekran ko�a ratunkowe
	private void printLifeLines() {
		String[] lifeLineNames = new String[3];
		for(int i = 0; i<lifeLines.size() ; i++) {
			lifeLineNames[i] = lifeLines.get(i).getClass().getName();
			lifeLineNames[i] = lifeLineNames[i].substring(10);
			if(lifeLineNames[i].contentEquals("FiftyFifty") ) {
				lifeLineNames[i] = "50-50";
			}
			if(lifeLineNames[i].contentEquals("ChangeOfCategory") ) {
				lifeLineNames[i] = "Zmiana Kategorii";
			}
			if(lifeLineNames[i].contentEquals("SecondChance") ) {
				lifeLineNames[i] = "Druga Szansa";
			}
			
		}
		
			for(int i=0 ; i<lifeLines.size(); i++) {
				Log.info((i+1)+") "+lifeLineNames[i]);
			}
			Log.info((lifeLines.size()+1)+") Pomijam Ko�o Ratunkowe");
		
	}
	//cecha sprawdzaj�ca poprawno�� podanego emaila
	@Override
	public String getNickName() {
		return getNickName(nickName);
	}
	

}
