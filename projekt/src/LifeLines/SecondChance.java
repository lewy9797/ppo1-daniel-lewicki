package LifeLines;

import Main.Log;
import java.util.ArrayList;
import QuestionsAndAnswers.*;
import java.util.Random;

public class SecondChance extends LifeLines {

	private static ArrayList<QuestionsAndAnswers> categories = new ArrayList();
	
	private Random r = new Random();
	
	public SecondChance() {
		fillCategoryList();
		this.categories = super.categories;
	}

	@Override
	public String[][] lifeLine(String[][] list, int question, QuestionsAndAnswers category) {
		
		Log.info("Wybra�e� ko�o ratunkowe: Druga Szansa");
		int newQuestion = r.nextInt(9);
		while(newQuestion == question) {
			newQuestion = r.nextInt(9);
		}
		super.newQuestion = newQuestion;
		String[][] newList = new String[9][5];
		for(int i=0 ; i<5; i++) {
			newList[newQuestion][i] = list[newQuestion][i];
		}
		return newList;	
	}
	
}
