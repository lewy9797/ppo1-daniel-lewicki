package LifeLines;

import QuestionsAndAnswers.*;
import java.util.ArrayList;
public abstract class LifeLines {

	protected ArrayList<QuestionsAndAnswers> categories = new ArrayList();
	protected int newQuestion = 0;
	public LifeLines() {}
	
	abstract public String[][] lifeLine(String[][] list, int question, QuestionsAndAnswers category);
	
	public int getNewQuestion() {
		return newQuestion;
	}
	protected void fillCategoryList(){
		
			  categories.add(new Art());
			  categories.add(new Biology());
			  categories.add(new Film());
			  categories.add(new Geography());
			  categories.add(new It());
			  categories.add(new MathClass());
			  categories.add(new Music());
			  categories.add(new Physics());
			  categories.add(new Sport());

	}
	
	public ArrayList<QuestionsAndAnswers> getCategories(){
		return categories;
	}
}
