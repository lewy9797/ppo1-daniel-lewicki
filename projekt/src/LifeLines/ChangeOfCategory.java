package LifeLines;

import QuestionsAndAnswers.*;
import java.util.Collections;
import Main.Log;

public class ChangeOfCategory extends LifeLines {

	public ChangeOfCategory() {
		fillCategoryList();
		
	}
	
	private QuestionsAndAnswers getLifeLine(QuestionsAndAnswers oldCategory) {
	
		fillCategoryList();
		String oldCategoryName = new String(oldCategory.getClass().getName());
		
		Collections.shuffle(categories);
		String newCategoryName = new String(categories.get(0).getClass().getName());
		while(oldCategoryName.contentEquals(newCategoryName)) {
			Collections.shuffle(categories);
			newCategoryName = categories.get(0).getClass().getName();
		}		
		return categories.get(0);
	}
	
	@Override
	public String[][] lifeLine(String[][] list, int question, QuestionsAndAnswers category) {
		
		Log.info("Wybra�es ko�o ratunkowe: Zmiana Kategorii");
		category = getLifeLine(category);
		categories.add(category);
		String[][] newList = new String[9][5];
		newList = category.getQuestionList();
		
		return newList;	
	}
	
}
