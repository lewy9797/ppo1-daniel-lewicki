package LifeLines;

import QuestionsAndAnswers.*;
import java.util.Random;
import Main.Log;

public class FiftyFifty extends LifeLines {
	
	
	private Random r = new Random();
	
	public FiftyFifty(){
		
		
	}
	
	public String[] getFiftyFifty(String[][] list, int question, QuestionsAndAnswers category) {
		//utworzenie listy odpowiedzi
		String[] newList = new String[4];
		
		for(int i = question ; i<question+1 ; i++) {
			for(int j=1 ; j<5 ; j++) {
				newList[j-1] = list[question][j];
			}
		}
		
		String correctAnswer = new String(category.getCorrectAnswer(question));
		int correctIteration = 0;
		//sprawdzanie kt�ry element jest prawidlow� odpowiedzi�
		for(int i=0 ; i<4 ; i++) {
			if(correctAnswer.contentEquals(newList[i]) ) {
				correctIteration = i;
			}
		}
		//tworzenie 50-50
		for(int i=0 ; i<4 ; i++) {
			if(i == correctIteration) {
				int one = r.nextInt(4), two = r.nextInt(4);
				while(one == correctIteration || one==two) {
					one = r.nextInt(4);					
				}
				
				while(two == correctIteration || one==two) {
					two = r.nextInt(4);
				}
				
				newList[one] = "";
				newList[two] = "";
			}
		}
		return newList;	
	}
	
	@Override
	public String[][] lifeLine(String[][] list, int question, QuestionsAndAnswers category) {
		
		Log.info("Wybra�es ko�o ratunkowe: 50-50");
		String[] helpList = new String[4];
		helpList = getFiftyFifty(list, question,category);
		String[][] newList = new String[9][5];
		for(int i=1 ; i<5 ; i++) {
			newList[question][i] = helpList[i-1];		
		}
		newList[question][0] = list[question][0];
		return newList;	
	}
}
