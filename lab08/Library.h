#ifndef LIBRARY_H
#define LIBRARY_H

#include <iostream>
#include <string>
#include <vector>


#include "section.h"

using namespace std;

class Library {
	
	public:
		Library(string name);
		
		Library& operator +=(const Section &section);
		
		vector<Section> getSection();
		void print();
		
	protected:
		string name;
		
		vector<Section> sections;
	
		
};

#endif
