#ifndef SECTION_H
#define SECTION_H

#include<iostream>
#include<string>
#include<vector>


#include "Book.h"

class Section{
	
	protected:
		string name;
		vector<Book> books;
		
	public:
		
		Section(string name);
		Section();
		Section& operator += (const Book& book);
		
		vector<Book> getBooks();
		string getName();
		void print();
		
		
		
	
};


#endif
