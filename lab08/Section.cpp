#include "Section.h"
#include "Library.h"
Section::Section(){
	
}

Section::Section(string name)
{
	this->name=name;
}

Section& Section::operator +=(const Book& book)
{
	this->books.push_back(book);
	return *this;
}


void Section::print()
{
	cout << this->name << endl;
	for(int i = 0; i < this->books.size(); i++) {
		this->books.at(i).print();
	}
}

vector<Book> Section::getBooks(){
	
	return books;
}

string Section::getName(){
	
	return this->name;
}
