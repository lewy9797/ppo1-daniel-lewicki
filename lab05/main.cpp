#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <string>

using namespace std;

class Book {
	protected:
		string title;
	
	public:			
		Book(string title) {
			this->title = title;
		}
		
		string getTitle() {
			return title;
		}
		
		virtual string getType() const = 0;
};

class Novel : public Book {

	public:	
		Novel(string title) : Book(title) {}
		
		string getType() const {
			return "powie��";
		}
	
};

class Comic : public Book{
	protected:
		string author;
		
		string graphDesigner;
		int pages;
		
	public:
		
		Comic(string title) : Book(title) {}
		
		string getType() const {
			return "komiks";
		}	
	
	
};

class Handbook: public Book{
	protected:
		string author;
		
		string publisher;
		int pages;
		
	public:
		Handbook(string title) : Book(title) {}
		
		string getType() const {
			return "podr�cznik";
		}	
	
	
};

class Library {
	public:
		vector<Library> books;
		vector<Novel> novels;
		vector<Comic> comics;
		vector<Handbook> handbooks;
		
		Library& addNovel(Novel novel) {
		
			this->novels.push_back(novel);
			
			return *this;
		}
		Library& addComic(Comic comic){
			this->comics.push_back(comic);
			return *this;
		}
		Library& addHandbook(Handbook handbook){
			this->handbooks.push_back(handbook);
			return *this;
		}
		
		Library& getNovel()
		{
			for(int i = 0; i < novels.size(); i++) {
				Novel novel = getNovels().at(i);
				cout << novel.getType() << " || " << novel.getTitle() << endl;
			
	    	}		
			
			return *this;
		}
		
		Library& getComic()
		{
			for(int i = 0; i < comics.size(); i++) {
				Comic comic = getComics().at(i);
				cout << comic.getType() << " || " << comic.getTitle() << endl;
			
	    	}		
			
			return *this;
		}
		
		Library& getHandbook()
		{
			for(int i = 0; i < handbooks.size(); i++) {
				Handbook handbook = getHandbooks().at(i);
				cout << handbook.getType() << " || " << handbook.getTitle() << endl;
			
	    	}		
			
			return *this;
		}
		
		vector<Novel> getNovels() {
			return novels;
		}
		vector<Comic> getComics() {
			return comics;
		}
		vector<Handbook> getHandbooks() {
			return handbooks;
		}
		
};

int main() {
	setlocale(LC_ALL, "");
	
	Library library = Library();
	library.addNovel(Novel("Wektor pierwszy")).addNovel(Novel("Gwiazda po gwie�dzie")).addNovel(Novel("Jednocz�ca Moc"));
	library.addComic(Comic("Spider-man")).addComic(Comic("Death Note")).addComic(Comic("Naruto"));
	library.addHandbook(Handbook("1001 sposob�w na zmarszczki")).addHandbook(Handbook("DIY")).addHandbook(Handbook("Jak wygl�da� dobrze po 40-stce"));
	
	cout<<"Nowele: "<<endl;
	library.getNovel();
	cout<<endl<<"Komiksy: "<<endl;
	library.getComic();
	cout<<endl<<"Poradniki: "<<endl;
	library.getHandbook();
	
	cout<<endl<<"Wykaz wszystkich ksi��ek: "<<endl<<endl;
	
	library.getNovel().getComic().getHandbook();
	
	
	
	
	return 0;
}
