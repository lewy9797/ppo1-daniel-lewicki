#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <math.h>
#include <string>
#include <sstream>

using namespace std;

class Log 
{
	public:
		static void info(string message = "") 
		{
			cout << message << endl;
		}
};

class Dice 
{
	public:
	int n;
	
		Dice()
		{
			
		}
		Dice(int n)
		{
			
		}
		
		
		void setSide(int n)
		{
			this->n=n;
		}		
		int roll() 
		{
			
			int result = (rand() % this->n) + 1;
			
			ostringstream ss;
			ss << "Dice roll: " << result;
			Log::info(ss.str());
			
			return result;
		}
};

class Pawn 
{
	public:
		int position;
		string name;
		
		Pawn() 
		{
			
		}
		
		Pawn(string name) 
		{
			this->name = name;
			this->position = 0;
			
			Log::info(name + " joined the game.");
		}
};

class Board 
{
	public:
		int maxPosition ;
		
		vector<Pawn> pawns;
		Dice dice;
		Pawn winner;
		int turnsCounter;
		
		Board() 
		{
			this->turnsCounter = 0;
		}
		void setMaxPosition(int maxPosition)
		{
			this->maxPosition=maxPosition;
			
		}
		
		void performTurn() 
		{
			this->turnsCounter++;
			
			ostringstream ss;
			ss << "Turn " << this->turnsCounter;
			Log::info();
			Log::info(ss.str());
			
			for(int i = 0; i < this->pawns.size(); i++) 
			{
				int rollResult = this->dice.roll();
				Pawn &pawn = this->pawns.at(i);
				if(rollResult==1 && pawn.position%2==0)
					{
						int penaltyDice = dice.roll();
						pawn.position-=penaltyDice;
					
					}

				if(rollResult==6 && pawn.position%10==0)
					{
						int bonusDice = dice.roll();
						pawn.position += bonusDice;
						
					}
					
				
				pawn.position += rollResult;
				
				ostringstream ss;
				ss << pawn.name << " new position: " << pawn.position;
				Log::info(ss.str());
								
				if(pawn.position >= this->maxPosition) 
				{
					this->winner = pawn;
					throw "Winner was called!";
				}
			}
		}
};



int main() {
	srand(time(NULL));
	

	
	
	Board board = Board();
	board.dice = Dice();
	cout<<"Set dice sides: "<<endl;
	int n;
	cin>>n;
	board.dice.setSide(n);
	
	cout<<"How many players? "<<endl;
	int players;
	cin>>players;
	if(!(players>2 && players <11))
	{
		cout<<"You must type 3-10 players!"<<endl;
		return 0;
	}
	string names[10] ={"Adam","Czesio","Przemek","Kinga","Natalia","�ukasz","Daniel","Justyna","Milena","Dariusz"}; 
	for(int i=0 ; i<players ; i++)
	{
		board.pawns.push_back(Pawn(names[i]));
	}
	cout<<"How many points?" <<endl;
	
	int points;
	cin>>points;
	while(!(points>99 && points <501))
	{
		cout<<"You must type 100-500 points!" << endl;
		cin>>points;
		
	}
	board.setMaxPosition(points);
	
	
	try 
	{
		while(true) 
		{
			board.performTurn();
		}
	}
	 
	catch(const char* exception) 
	{
		Log::info();
		Log::info(board.winner.name + " won");
	}
	
	return 0;
}

