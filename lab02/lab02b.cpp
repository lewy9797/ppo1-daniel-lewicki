#include <iostream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>
 
using namespace std;
 
#define STUDENTS_COUNT 10
 
class Student {
    public:
        string studentNo;
        string studentName, studentSurname,isStudentActive;
           
        void setStudentNo(string studentNo) {
            this->studentNo = studentNo;
        }
        void setStudentName(string studentName){
            this->studentName=studentName;
        }
        void setStudentSurname(string studentName){
            this->studentSurname=studentName;
        }
        void setStudentActive(string isStudentActive){
            this->isStudentActive=isStudentActive;
        }
        string getStudentNo() {
            return this->studentNo;
        }
        string getStudentName() {
            return this->studentName;
        }
        string getStudentSurname() {
            return this->studentSurname;
        }
        string getStudentActive() {
            return this->isStudentActive;
        }
};
 
string getRandomStudentNumber() {
    ostringstream ss;
    int randomNumber = rand() % 2000 + 37000;
   
    ss << randomNumber;
   
    return ss.str();
}
 
string getRandomStudentName(){
    ostringstream ss;
    string Names[10]={"Adam ","Andrzej ","Zdzis�aw ","Przemek ","Daniel ","Kinga ","Natalia ","�ukasz ","Piotr ","Darek "};
    string Name= Names[rand()%9];
    ss<< Name;
    return ss.str();
}
 
string getRandomStudentSurname(){
    ostringstream ss;
    string Names[10]={"Adamowski ","Andrzejowski ","Zdzis�awowski ","Przemekowski ","Danielowicz ","Kingowska ","Natalikowa ","�ukaszowy ","Piotrowy ","Darowy "};
    string Name= Names[rand()%9];
    ss<< Name;
    return ss.str();
}
string getStudentActive()
{
	ostringstream ss;
	int i = rand()%2;
	ss<<i;
	return ss.str();
}
int main() {
    vector<Student> students;
   
    for(int i = 0; i < STUDENTS_COUNT; i++) {
        Student student;
        student.setStudentNo(getRandomStudentSurname()+getRandomStudentName()+"("+getRandomStudentNumber()+")"+getStudentActive());
        students.push_back(student);
    }
   
    cout  << "Students group have been filled." << endl << endl;
   
    for(int i = 0; i < students.size(); i++) {
        Student student = students.at(i);
        cout << student.getStudentNo() << endl;
    }
   
    return 0;
}
