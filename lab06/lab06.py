import random
from force_users import *

print("How many Players?")
nr = int(input())

champions_names = [
        "Jaden Korr", "A'Sharad Hett", "Aayla Secura", "Asajj Ventress", "Luke Skywalker", "Emperor Palpatine",
        "Darth Vader"
    ]

champions_classes = [

    LightsaberUser(random.choice(champions_names)),
    LightSideForceUser(random.choice(champions_names)),

    DarkSideForceUser(random.choice(champions_names)),
    JediMaster(random.choice(champions_names)),

    SithLord(random.choice(champions_names)),

]

champions = []

i=0
while i < nr:
    new_champ = random.choice(champions_classes)
    old_champ = new_champ

    if new_champ.name == old_champ.name:
        new_champ = random.choice(champions_classes)
        champions.extend([new_champ])
        i = i+1



while len(champions) > 1:
    attacker = random.choice(champions)
    target = attacker

    while attacker == target:
        target = random.choice(champions)

    attacker.attack(target)
    if target.life_points <= 0:
        print(target.name + " died.")

        champions.remove(target)

print("")
print("The winner is " + champions[0].name)

