import random


class ForcePower(object):
    def __init__(self, name, damage):
        self.name = name
        percent_damage = 0.1 * damage
        damage = damage + random.uniform(-percent_damage, percent_damage)

        self.damage = float(damage)

    def __str__(self):
        return self.name + ": " + str(self.damage)

class ForceDefense(object):
    def __init__(self, name, defense ):
        self.name = name
        self.defense = defense



    def __str__(self):
        return self.name + ": " + str(self.defense) + "defence points."

    def defense(self, attack):
        attack = attack - self.defense*attack
        return attack


class ForceUser(object):

    life_points = 100
    available_movements = []

    def __init__(self, name):
        self.name = name
        self.available_movements = []

    def attack(self, opponent):

        drawnMovement = random.choice(self.available_movements)

        opponent.life_points -= drawnMovement.damage

        print(self.name + " attacks " + opponent.name + " with " + drawnMovement.name + ".")
        print(opponent.name + " now has " + str(opponent.life_points) + " life points.")




class LightsaberUser(ForceUser):

    def __init__(self, name ):
        ForceDefense(name , 0.4)
        super(LightsaberUser, self).__init__(name)

        self.available_movements.extend([
            ForcePower("Lightsaber Attack", 10),
            ForcePower("Lightsaber Throw", 15)

        ])


class LightSideForceUser(ForceUser):
    def __init__(self , name):
        ForceDefense(name, 0.5)
        super(LightSideForceUser, self).__init__(name)

        self.available_movements.extend([
            ForcePower("Mind Trick", 5)

        ])


class JediMaster(LightsaberUser , LightSideForceUser):
    def __init__(self , name):
        ForceDefense(name, 0.8)

        super(JediMaster,self).__init__(name)

        self.available_movements.extend([
            ForcePower("Force Light", 35)

        ])


class DarkSideForceUser(ForceUser):
    def __init__(self , name ):
        ForceDefense(name, 0.65)
        super(DarkSideForceUser, self).__init__(name)

        self.available_movements.extend([
            ForcePower("Force Grip", 15),
            ForcePower("Force Lightning", 15)

        ])


class SithLord(DarkSideForceUser , LightsaberUser):
    def __init__(self , name):
        ForceDefense(name, 0.2)
        super(SithLord, self).__init__(name)

        self.available_movements.extend([
            ForcePower("Force Rage", 30)

            ])