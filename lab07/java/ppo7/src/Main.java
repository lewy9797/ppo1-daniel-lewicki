import ParkingLotManager.Interfaces.EntityInterface;
import ParkingLotManager.Log;
import ParkingLotManager.ParkingLot;
import ParkingLotManager.QueueGenerator;
import ParkingLotManager.TicketMachine;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
    	int dayTime = 0;
    	TicketMachine ticketFriend = new TicketMachine();
    	while(dayTime < 3) {
    		switch(dayTime) {
    			case 0: 
    				Log.info("It's 8.00 o'clock----------------------------------------");
    				break;
    			case 1:
    				Log.info("It's 15.00 o'clock----------------------------------------");
    				break;
    			case 2:
    				Log.info("It's 19.00 o'clock----------------------------------------");
    				break;
    		}
    		
    		QueueGenerator generator = new QueueGenerator();
            ArrayList<EntityInterface> queue = generator.generate();
            ParkingLot parking = new ParkingLot();

            Log.info("There's " + parking.countCars() + " cars in the parking lot");
            Log.info();

            for (EntityInterface entity : queue) {
                if(parking.checkIfCanEnter(entity)) {
                	ticketFriend.setTakings(entity.getMoney(), entity,parking.countBikes());
                    parking.letIn(entity);
                    
                    
                }
            }

            Log.info();
            Log.info("There's " + parking.countCars() + " cars in the parking lot");
            Log.info("There's " + parking.countBikes() + " bikes in the parking lot");
            Log.info();
            dayTime++;
    	}
    	Log.info("End of day, time to rest!");
    	Log.info("Takings: "+ticketFriend.getTakings()+" cebulions.");
    	
    }
}
