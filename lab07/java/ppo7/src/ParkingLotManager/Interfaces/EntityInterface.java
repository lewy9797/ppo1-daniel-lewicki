package ParkingLotManager.Interfaces;

public interface EntityInterface {

    String identify();
    boolean canEnter();
    void setEntry(boolean entry);
    void setMoney(int money);
    int getMoney();

}
