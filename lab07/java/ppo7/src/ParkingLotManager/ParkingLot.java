package ParkingLotManager;

import ParkingLotManager.Entities.Bicycle;
import ParkingLotManager.Entities.Car;
import ParkingLotManager.Entities.Courier;
import ParkingLotManager.Entities.PrivilegedVehicles;
import ParkingLotManager.Entities.Tank;
import ParkingLotManager.Entities.TeacherCar;
import ParkingLotManager.Interfaces.EntityInterface;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.util.ArrayList;

final public class ParkingLot {

    private ArrayList<EntityInterface> entitiesOnProperty = new ArrayList<>();
    private int carsOnProperty = 0;
    private int bikesOnProperty = 0;
    private ArrayList<String> blackList = new ArrayList();
    

    public boolean checkIfCanEnter(EntityInterface entity) {
    	fillBlackList();
    	if(isParkingFull(entity)) {
    		entity.setEntry(false);
    		if(entity instanceof PrivilegedVehicles || entity instanceof Courier) {
    			entity.setEntry(true);
    		}
    	}
    	if(isOnBlackList(entity)) {
    		Log.info("You're under arrest, you have the right to maintain your silence...");
    		entity.setEntry(false);
    	}
    	if(!isMoneyGoodEnough(entity.getMoney(),entity)) {
    		entity.setEntry(false);
    		
    		
    	}
    	if(entity instanceof Tank) {
    		Log.info("You're too fat!");
    	}
    	
    	
        return entity.canEnter();
    }

    public void letIn(EntityInterface entity) {
        entitiesOnProperty.add(entity);
        Log.info(entity.getClass().getName().substring(27)+entity.identify() + " let in.");

        if(entity instanceof Car) {
            carsOnProperty++;
        }
        if(entity instanceof Bicycle) {
        	bikesOnProperty++;
        }
    }

    public int countCars() {
        return carsOnProperty;
    }
    
    public int countBikes() {
    	return bikesOnProperty;
    }
    
    private boolean isMoneyGoodEnough(int money , EntityInterface entity) {
    	boolean isOk = false;
    	if(money >= 2) {
    		isOk = true;
    	}
    	else {
    		Log.info("Go to work "+entity.getClass().getName().substring(27)+"!");
    	}
    	return  isOk;
    }
    
    private boolean isParkingFull(EntityInterface entity) {
    	boolean check = false;
    	if(entity instanceof Car || entity instanceof TeacherCar) {
    		if(carsOnProperty >= 100) {
        		Log.info("Parking is full!");
        		check = true;		
        	}else {
        		check = false;
        	}
    	}
    		
    	return check;
    }
    
    private void fillBlackList(){
    	for (int i = 0 ; i<100 ; i++) {
    		blackList.add("DLX " + (10000+i*3));
    	}
    	
    }
    
    private boolean isOnBlackList(EntityInterface entity) {
    	
    	boolean check = false;
    	for(int i=0 ; i<100 ; i++) {
    		Pattern list = Pattern.compile(blackList.get(i));
    		Matcher matcher = list.matcher(entity.identify());
    		if(matcher.find()) {
    			check = true;
    		}
    	}
    	return check;
    	
    }

}
