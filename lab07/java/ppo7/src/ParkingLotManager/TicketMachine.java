package ParkingLotManager;

import ParkingLotManager.Interfaces.EntityInterface;
import ParkingLotManager.Entities.*;
public class TicketMachine {
	
	private int takings = 0;
	
	public TicketMachine() {
		
	}
	
	public int getTakings() {
		return takings;
	}
	
	
	public void setTakings(int money, EntityInterface entity,int bikesOnProperty) {
		if(entity instanceof Pedestrian || entity instanceof TeacherCar || entity instanceof PrivilegedVehicles || entity instanceof Courier) {
			Log.info("You're too cool for pay, "+entity.getClass().getName().substring(27)+"!");
		}else if(entity instanceof Bicycle || bikesOnProperty >=15) {
			takings+=2;
			entity.setMoney(entity.getMoney()-2);
		}else {
			takings+=2;
			entity.setMoney(entity.getMoney()-2);
		}
		
	}
}
