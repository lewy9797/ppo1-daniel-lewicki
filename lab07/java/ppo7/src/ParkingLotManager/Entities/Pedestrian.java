package ParkingLotManager.Entities;

import ParkingLotManager.Interfaces.EntityInterface;
import java.util.Random;

public class Pedestrian implements EntityInterface {

	private Random generator = new Random();
    private String name = "";
    private boolean entry = true;
    private int money = generator.nextInt(10);
    public Pedestrian() {}

    public Pedestrian(String name) {
        this.name = name;
    }

    public String identify() {
        return !name.isEmpty() ? name : " with no-name";
    }

    public boolean canEnter() {
        return entry;
    }
    
    public void setEntry(boolean entry) {
    	this.entry = entry;
    }

	@Override
	public void setMoney(int money) {
		this.money = money;
		
	}

	@Override
	public int getMoney() {
		return money;
	}

}
