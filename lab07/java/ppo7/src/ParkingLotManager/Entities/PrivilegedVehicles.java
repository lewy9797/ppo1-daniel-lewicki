package ParkingLotManager.Entities;

import ParkingLotManager.Interfaces.EntityInterface;
import java.util.Random;
public class PrivilegedVehicles implements EntityInterface{

	private Random generator = new Random();
	private String name;
	private int money = generator.nextInt(10);
	private boolean entry = true;
	
	public PrivilegedVehicles(String name) {
		this.name = name;
	}
	
	@Override
	public String identify() {	
		return name;
	}

	@Override
	public boolean canEnter() {
		return entry;
	}

	@Override
	public void setEntry(boolean entry) {
		this.entry = entry;
	}

	@Override
	public void setMoney(int money) {
		this.money = money;
	}

	@Override
	public int getMoney() {
		return money;
	}

}
