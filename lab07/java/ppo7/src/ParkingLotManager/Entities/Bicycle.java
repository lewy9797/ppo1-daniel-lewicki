package ParkingLotManager.Entities;

import ParkingLotManager.Interfaces.EntityInterface;
import java.util.Random;
public class Bicycle implements EntityInterface {

	private boolean entry = true;
	private Random generator = new Random();
	private int money = generator.nextInt(10)+2;
    public String identify() {
        return " with no name";
    }

    public boolean canEnter() {
        return entry;
    }
    
    public void setEntry(boolean entry) {
    	this.entry = entry;
    }
    
    public int getMoney() {
    	return money;
    }
    
    public void setMoney(int money) {
    	this.money = money;
    }

}
