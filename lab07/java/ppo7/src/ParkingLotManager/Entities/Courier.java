package ParkingLotManager.Entities;

import java.util.Random;
import ParkingLotManager.Interfaces.EntityInterface;

public class Courier implements EntityInterface{

	private Random generator = new Random();
	private String name;
	private int money = generator.nextInt(10);
	private boolean entry = true;
	
	public Courier(String name) {
		this.name = name;
	}
	@Override
	public String identify() {	
		return name;
	}

	@Override
	public boolean canEnter() {
		return entry;
	}

	@Override
	public void setEntry(boolean entry) {
		this.entry = entry;
	}

	@Override
	public void setMoney(int money) {
		this.money = money;
	}

	@Override
	public int getMoney() {
		return money;
	}

}
