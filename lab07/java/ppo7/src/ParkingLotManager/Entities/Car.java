package ParkingLotManager.Entities;

import ParkingLotManager.Interfaces.EntityInterface;
import java.util.Random;

public class Car implements EntityInterface {

    private String plate;
    private Random generator = new Random();
    private int money = generator.nextInt(10);
    private boolean entry = true;
    
    public Car(String plate) {
        this.plate = plate;
    }

    public String identify() {
        return " with plate number " + plate;
    }

    public boolean canEnter() {
        return entry;
    }
    
    public void setEntry(boolean entry) {
    	this.entry = entry;
    }
    
    public int getMoney() {
    	return money;
    }
    
    public void setMoney(int money) {
    	this.money = money;
    }

}
