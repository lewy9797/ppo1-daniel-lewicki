package ParkingLotManager;

import ParkingLotManager.Entities.Bicycle;
import ParkingLotManager.Entities.Car;
import ParkingLotManager.Entities.Courier;
import ParkingLotManager.Entities.Pedestrian;
import ParkingLotManager.Entities.PrivilegedVehicles;
import ParkingLotManager.Entities.Tank;
import ParkingLotManager.Entities.TeacherCar;
import ParkingLotManager.Interfaces.EntityInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class QueueGenerator {

    private static int ANONYMOUS_PEDESTRIANS_COUNT = 0;
    private static int PEDESTRIANS_COUNT = 0;
    private static int CARS_COUNT = 0;
    private static int TEACHER_CARS_COUNT = 0;
    private static int BICYCLES_COUNT = 0;
    private static int COURIERS_COUNT = 0;
    private static int PRIVILEGED_CARS_COUNT = 0;
    private static int TANKS_COUNT = 0;
    private Random generator = new Random();
    
    public QueueGenerator() {
    	ANONYMOUS_PEDESTRIANS_COUNT = generator.nextInt(91);
    	PEDESTRIANS_COUNT = generator.nextInt(101);
    	CARS_COUNT = generator.nextInt(101);
    	TEACHER_CARS_COUNT = generator.nextInt(51);
    	BICYCLES_COUNT = generator.nextInt(31);
    	COURIERS_COUNT = generator.nextInt(5);
    	PRIVILEGED_CARS_COUNT = generator.nextInt(5);
    	TANKS_COUNT = generator.nextInt(5);
    }
    public ArrayList<EntityInterface> generate() {
        ArrayList<EntityInterface> queue = new ArrayList<>();

        for (int i = 0; i <= ANONYMOUS_PEDESTRIANS_COUNT; i++) {
            queue.add(new Pedestrian());
        }

        for (int i = 0; i <= PEDESTRIANS_COUNT; i++) {
            queue.add(new Pedestrian(getRandomName()));
        }

        for (int i = 0; i <= CARS_COUNT; i++) {
            queue.add(new Car(getRandomPlateNumber()));
        }

        for (int i = 0; i <= TEACHER_CARS_COUNT; i++) {
            queue.add(new TeacherCar(getRandomPlateNumber()));
        }

        for (int i = 0; i <= BICYCLES_COUNT; i++) {
            queue.add(new Bicycle());
        }
        
        for (int i = 0; i <= COURIERS_COUNT; i++) {
            queue.add(new Courier(getRandomCourierName()));
        }
        
        for (int i = 0; i <= PRIVILEGED_CARS_COUNT; i++) {
            queue.add(new PrivilegedVehicles(getRandomPrivilegedVehiclesName()));
        }
        
        for (int i = 0; i <= TANKS_COUNT; i++) {
            queue.add(new Tank(getRandomName()));
        }

        Collections.shuffle(queue);

        return queue;
    }

    private String getRandomPlateNumber() {
        Random generator = new Random();
        return "DLX " + (generator.nextInt(89999) + 10000);
    }

    private String getRandomName() {
        String[] names = {" John", " Jack", " James", " George", " Joe", " Jim"};
        return names[(int) (Math.random() * names.length)];
    }
    
    private String getRandomCourierName() {
        String[] names = {" DHL", " Postal Plus", " Letters&Nothing Matters"};
        return names[(int) (Math.random() * names.length)];
    }
    
    private String getRandomPrivilegedVehiclesName() {
        String[] names = {" Police", " Ambulance", " FireTruck"};
        return names[(int) (Math.random() * names.length)];
    }

}
