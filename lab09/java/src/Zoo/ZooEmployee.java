package Zoo;
import Food.*;
public class ZooEmployee {
	private String name;
	private static boolean canFeed = false;
	
	public ZooEmployee() {
		
	}
	public ZooEmployee(String name) {
		this.name=name;
		Log.info(name + " has been recruited!");
	}
	
	public static void setCanFeed(boolean feed) {
		canFeed=feed;
	}
	
	public boolean getCanFeed() {
		return canFeed;
	}
	
	public Food feedingPlan(int hour) {
		Food food = null;
		if(hour==8) {
			
			food = new Meat();
		}
		else if(hour==9) {
			
			food = new Milk();
		}
		else if(hour == 12) {
			
			food = new Water();
		}
		else if(hour == 15) {
			
			food = new Fruits();
		}
		else if(hour == 13) {
			
			food = new Vegetables();
		}
		
		
		return food;
		
		
	}
	
	
	
	
	

}
