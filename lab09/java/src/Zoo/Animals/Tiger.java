package Zoo.Animals;

import java.util.ArrayList;

import Food.*;

public class Tiger extends Animal{

	public Tiger(String name) {
		super(name);
	}

	@Override
	ArrayList<Food> getDiet() {
    	ArrayList<Food> food = new ArrayList();
    	food.add(new Milk());
    	food.add(new Water());
    	food.add(new Meat());
    	
        return food;
    }
}
