package Zoo.Animals;

import java.util.ArrayList;

import Food.*;

public class Penguin extends Animal{

	public Penguin(String name) {
		super(name);
	}

	@Override
	ArrayList<Food> getDiet() {
    	ArrayList<Food> food = new ArrayList();
    	food.add(new Fruits());
    	food.add(new Water());
    	food.add(new Vegetables());
    	food.add(new Meat());
    	
        return food;
    }
}
