package Zoo.Animals;
import java.util.ArrayList;

import Food.*;
public class Lion extends Animal {

    public Lion(String name) {
        super(name);
    }

    @Override
    ArrayList<Food> getDiet() {
    	ArrayList<Food> food = new ArrayList();
    	food.add(new Meat());
    	food.add(new Water());
    	food.add(new Milk());
    	
        return food;
    }

}
