package Zoo.Animals;

import java.util.ArrayList;

import Food.Food;
import Food.*;

public class Zebra extends Animal{

	public Zebra(String name) {
        super(name);
    }

    @Override
    ArrayList<Food> getDiet() {
    	ArrayList<Food> food = new ArrayList();
    	food.add(new Fruits());
    	food.add(new Water());
    	food.add(new Vegetables());
    	
        return food;
    }
}
