package Zoo.Animals;
import Food.*;
import Zoo.Log;

import java.util.ArrayList;
import java.util.Arrays;

abstract public class Animal {

    private String name;
    private boolean hungry = true;

    Animal(String name) {
        this.name = name;
    }

    abstract ArrayList<Food> getDiet();

    public void feed(Food food) throws Exception {
    	
    	String food1 = food.getClass().getSimpleName();
    	String[] food2 = new String[getDiet().size()];
    	for(int i=0 ; i<getDiet().size(); i++) {
    		food2[i]=getDiet().get(i).getClass().getSimpleName();
    	}
		if(!Arrays.asList(food2).contains(food1)) {
            throw new Exception(getName() + " doesn't eat " + food1 + ".");
        }

        hungry = true;
        Log.info(getName() + " fed.");
    }

    public String getName() {
        return getSpecies() + " " + name;
    }

    private String getSpecies() {
        return getClass().getSimpleName();
    }

}
