import Zoo.*;
import Zoo.Animals.*;
import Food.*;
public class Main {

    public static void main(String[] args) {
        Zoo zoo = new Zoo("Zoo Legnica");
        Clock clock = new Clock();
        ZooEmployee employee1 = new ZooEmployee("Andrzej");

        zoo.addAnimal(new Lion("Simba"))
            .addAnimal(new Lion("Mufasa"))
            .addAnimal(new Elephant("Dumbo"))
            .addAnimal(new Tiger("Daniel"))
            .addAnimal(new Zebra("Katey"))
            .addAnimal(new Chimpanzee("Jareczek"))
            .addAnimal(new Giraffe("Genowefa"))
            .addAnimal(new Porcupine("Bronis�aw"))
            .addAnimal(new Penguin("Kowalski"))
            .addAnimal(new Dolphin("Filipek"));

        
        clock.setHour(8);
        try {
        	zoo.feedAnimals(employee1.feedingPlan(clock.getHour()));
        }catch(Exception e) {
        	Log.warning(e.getMessage());
        }
        
       
    }
}
