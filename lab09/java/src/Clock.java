import Zoo.*;

public class Clock {
	
	private int hour;
	public Clock() {
		setHour(0);
	}
	
	
	
	public int getHour() {
		return this.hour;
	}
	
	public void setHour(int hour) {
		
		if(hour<1 && hour>24) {
			Log.info("Wrong hour!");
		}
		else {
			this.hour = hour;
		}
		if(hour==8 || hour==9 || hour==12 || hour==13 || hour==15) {
			ZooEmployee.setCanFeed(true);
		}
		else {
			ZooEmployee.setCanFeed(false);
		}
		
	}

}
